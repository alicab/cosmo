var express = require('express');
var router = express.Router();
var path = require('path');
const db = require(path.join(__dirname, '../','src/database'))
const Cosmonaut = require(path.join(__dirname, '../', 'src/models/cosmonaut'))

//get all cosmonauts
router.get('/', function (req, res, next){
Cosmonaut.find({}, function(err, cosmonauts) {
    if (err) return console.error(err);
    res.json(cosmonauts);  
  });
});

//post new cosmonaut
router.post('/new', function(req, res, next){
    var cosmonaut = new Cosmonaut({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    dateofbirth: req.body.dateofbirth,
    superpower: req.body.superpower,
    imgName: req.body.imgName
  });
  cosmonaut.save()
    .then(user => {
      res.status(201).json(cosmonaut);
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});

//delete cosmonaut
router.delete('/:id', function(req, res, next){
    Cosmonaut.findByIdAndRemove(req.params.id, (err, cosmonaut) => {
    if (err) return res.status(500).send(err);
    const response = {
        message: "Cosmonaut successfully deleted",
        id: cosmonaut._id
    };
    return res.status(200).send(response);
});
});

//update new cosmonaut
router.put('/:id', function(req, res, next){
    Cosmonaut.findByIdAndUpdate(
    req.params.id,
    req.body,
    {new: true},
    (err, cosmonaut) => {
        if (err) return res.status(500).send(err);
        return res.send(cosmonaut);
    }
)
});

module.exports = router;