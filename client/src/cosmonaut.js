import React from 'react';
import './App.css';
import { Card, CardBody, CardTitle, Button } from 'reactstrap';
import EditCosmonaut from './editcosmonaut.js'
import DeleteCosmonaut from './deletecosmonaut.js'

class Cosmonaut extends React.Component {

	constructor(props) {
    super(props);
    this.state = {
      editmode: false,
      deletemode: false,
      firstname: this.props.firstname,
      lastname: this.props.lastname,
      dateofbirth: this.props.dateofbirth,
      superpower: this.props.superpower,
      imgName: this.props.imgname,
      imgSrc: this.props.imgname ? 'https://res.cloudinary.com/dazg4q7xb/image/upload/'+this.props.imgname : 'cosmonaut.jpeg',
      
   };
  }

  changeStateEdit = () => {
  	this.setState({
            editmode: true
         });
  }

  changeStateNotEdit = () => {
  	this.setState({
            editmode: false
         });
  }

  changeStateDelete = () => {
  	this.setState({
            deletemode: true
         });
  }

  changeStateNotDelete = () => {
  	this.setState({
            deletemode: false
         });
  }

  changeStateData = (firstname, lastname, dateofbirth, superpower, imgname) => {
  	this.setState({
  			editmode: false,
  			updateLoading: true,
     		firstname: firstname,
      		lastname: lastname,
      		dateofbirth: dateofbirth,
      		superpower: superpower,
      		imgName: imgname,
      		imgSrc: imgname ? 'https://res.cloudinary.com/dazg4q7xb/image/upload/'+ imgname : 'cosmonaut.jpeg'
         }, ()=> {
         	this.props.getcosmonauts();
         });
  }

  rendercosmo(){
    return (
      	<React.Fragment>
      	<Card className='full padding_top' >
      	<div style={{backgroundImage: "url(" + this.state.imgSrc + ")"}} className='card_image'>
        </div>
        <CardBody>
          <CardTitle><b>Firstname:</b> {this.state.firstname}</CardTitle>
          <CardTitle><b>Lastname:</b> {this.state.lastname}</CardTitle>
          <CardTitle><b>Date of birth:</b> {this.state.dateofbirth}</CardTitle>
          <CardTitle><b>Superpower:</b> {this.state.superpower}</CardTitle>
        </CardBody>
        <div className='row padding_bottom' >
          <div className='col-2' ></div>
          <Button className="text-center col-4" onClick={this.changeStateEdit} >Edit</Button>
          <Button className="text-center col-4" onClick={this.changeStateDelete} >Delete</Button>
        </div>
      </Card>
      </React.Fragment>
      )
  }

  renderupdatecosmo(){
    return (
    	<EditCosmonaut className='full'
    			id={this.props.id}
    			firstname={this.state.firstname} 
    			lastname={this.state.lastname}
    			dateofbirth={this.state.dateofbirth}
    			superpower={this.state.superpower}
    			imgname={this.state.imgName} 
    			imgsrc={this.state.imgSrc} 

    			changestatedata={this.changeStateData}
    			changestatenotedit={this.changeStateNotEdit}/>
      )
  }

  renderdeletecosmo(){
    return (
    	<DeleteCosmonaut className='full delete_height'
    			id={this.props.id}  
    			getcosmonauts={this.props.getcosmonauts}
    			changestatenotdelete={this.changeStateNotDelete}/>
      )
  }

render() { 
      	if(this.state.editmode)
      		return (this.renderupdatecosmo())
      	else if(this.state.deletemode)
      		return (this.renderdeletecosmo())
      	else
      		return (this.rendercosmo())

 }
}
export default Cosmonaut