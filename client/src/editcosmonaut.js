import React from 'react';
import './App.css';
import { Card, CardBody, Button, Form, FormGroup, Input } from 'reactstrap';
import { IoIosAddCircle, IoIosCloseCircle } from 'react-icons/io';

class EditCosmonaut extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstname: this.props.firstname,
      lastname: this.props.lastname,
      dateofbirth: this.props.dateofbirth,
      superpower: this.props.superpower,
      imgName: this.props.imgname,
      firstnameForm: true,
      lastnameForm: true,
      dateofbirthForm: true,
      superpowerForm: true,
      firstnameFormClass: '',
      lastnameFormClass: '',
      dateofbirthFormClass: '',
      superpowerFormClass: '',
      updateloading: this.props.updateloading,
      imgurl: this.props.imgsrc,
      image: null,
      imageNew: false,
      removeOldImg: false,
      isOpacity: true
   };
  }

   updateCosmonaut = () => { 
    if(this.state.firstnameFormClass===''){
      this.setState({
            firstnameFormClass: 'greenForm'
         });
    }
    if(this.state.lastnameFormClass===''){
      this.setState({
            lastnameFormClass: 'greenForm'
         });
    }
    if(this.state.dateofbirthFormClass===''){
      this.setState({
            dateofbirthFormClass: 'greenForm'
         });
    }
    if(this.state.superpowerFormClass===''){
      this.setState({
            superpowerFormClass: 'greenForm'
         });
    }
    if(this.state.firstnameForm && this.state.lastnameForm && this.state.dateofbirthForm && this.state.superpowerForm){ 
  if(this.state.image){
  const formData = new FormData();
  formData.append("upload_preset", "hqc427x0");
  formData.append("file", this.state.image);
  formData.append("tags", "cosmo");
  var imgName=Date.now()+Math.floor(Math.random() * 101).toString();
  formData.append("public_id", imgName); 

const options = {
  method: 'POST',
  body: formData
};

fetch('https://api.cloudinary.com/v1_1/dazg4q7xb/image/upload/', options)
  .then((res) => {
    return res.json(); 
  })
  .then((res) => {
    console.log(res)
    fetch('/cosmonauts/'+this.props.id, {
      method: 'PUT',
      body: JSON.stringify({
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        dateofbirth: this.state.dateofbirth,
        superpower: this.state.superpower,
        imgName: imgName
      }),
      headers: {
        'Content-Type': 'application/json'
    }
    })
    .then(
      (res) => {
        this.props.changestatedata(this.state.firstname, this.state.lastname, this.state.dateofbirth, this.state.superpower, imgName);
  })
  .catch(function (error) {
    console.log(error);
  });
})
}else{
  if(this.state.removeOldImg){
  fetch('/cosmonauts/'+this.props.id, {
      method: 'PUT',
      body: JSON.stringify({
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        dateofbirth: this.state.dateofbirth,
        superpower: this.state.superpower,
        imgName: ''
      }),
      headers: {
        'Content-Type': 'application/json'
    }
    })
    .then(
      (res) => {
        this.props.changestatedata(this.state.firstname, this.state.lastname, this.state.dateofbirth, this.state.superpower, '');
  })
  .catch(function (error) {
    console.log(error);
  });
}
else{
  fetch('/cosmonauts/'+this.props.id, {
      method: 'PUT',
      body: JSON.stringify({
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        dateofbirth: this.state.dateofbirth,
        superpower: this.state.superpower
      }),
      headers: {
        'Content-Type': 'application/json'
    }
    })
    .then(
      (res) => {
        this.props.changestatedata(this.state.firstname, this.state.lastname, this.state.dateofbirth, this.state.superpower, this.props.imgname);
  })
  .catch(function (error) {
    console.log(error);
  });
}
}
}
}
  removeImage = () => {
    if(this.state.imageNew && !this.state.removeOldImg){ //ak je novy obrazok aj stary
        this.setState({
            image: null,
            imgurl: this.props.imgsrc,
            imageNew: false,
            isOpacity: true
         });
      }
    else if(this.state.imageNew && this.state.removeOldImg){ //ak je novy obrazok ale stary sa predtym vymazal
      this.setState({
            image: null,
            imgurl: 'cosmonaut.jpeg',
            imageNew: false,
            isOpacity: true
         });
    }else{ //ak je iba stary  if(this.state.removeOldImg)
        this.setState({
            image: null,
            imgName: '',
            imgurl: 'cosmonaut.jpeg',
            removeOldImg: true,
            isOpacity: true
         });
      }
}

  saveUpdateButton = () => {
    this.updateCosmonaut();
  }

  dismissUpdateButton = () => {
    this.props.changestatenotedit();
  }

  changeFirstnameInput = (event) => {
    if(event.target.value===''){
      this.setState({
            firstname: this.props.firstname,
            firstnameForm: true,
            firstnameFormClass: 'greenForm'
         });
    }else{
      this.setState({
            firstname: event.target.value
         }, ()=>{
          this.checkFirstnameInput(this.state.firstname);
         });
    }
  }

  changeLastnameInput = (event) => {
    if(event.target.value===''){
      this.setState({
            lastname: this.props.lastname,
            lastnameForm: true,
            lastnameFormClass: 'greenForm'
         });
    }else{
      this.setState({
            lastname: event.target.value
         }, ()=>{
          this.checkLastnameInput(this.state.lastname);
         });
    }
  }

  changeDateofbirthInput = (event) => {
    if(event.target.value===''){
      this.setState({
            dateofbirth: this.props.dateofbirth,
            dateofbirthForm: true,
            dateofbirthFormClass: 'greenForm'
         });
    }else{
      this.setState({
            dateofbirth: event.target.value
         }, ()=>{
          this.checkDateofbirthInput(this.state.dateofbirth);
         });
    }
  }

  changeSuperpowerInput = (event) => {
    if(event.target.value===''){
      this.setState({
            superpower: this.props.superpower,
            superpowerForm: true,
            superpowerFormClass: 'greenForm'
         });
    }else{
      this.setState({
            superpower: event.target.value
         }, ()=>{
          this.checkSuperpowerInput(this.state.superpower);
         });
    }
  }

  changeImgInput = (event) => {
    this.setState({
            image: event.target.files[0],
            imageNew: true,
            imgurl: URL.createObjectURL(event.target.files[0]),
            isOpacity: false
         });
  }

  checkFirstnameInput = (value) => {
  if(/^[a-zA-Z0-9 ]{1,20}$/.test(value)){
    this.setState({
            firstnameForm: true,
            firstnameFormClass: 'greenForm'
         });
  }else{
    this.setState({
            firstnameForm: false,
            firstnameFormClass: 'redForm'
         });
  }
}

checkLastnameInput = (value) => {
  if(/^[a-zA-Z0-9 ]{1,20}$/.test(value)){
    this.setState({
            lastnameForm: true,
            lastnameFormClass: 'greenForm'
         });
  }else{
    this.setState({
            lastnameForm: false,
            lastnameFormClass: 'redForm'
         });
  }
}

checkSuperpowerInput = (value) => {
  if(/^[a-zA-Z0-9 ]{1,20}$/.test(value)){
    this.setState({
            superpowerForm: true,
            superpowerFormClass: 'greenForm'
         });
  }else{
    this.setState({
            superpowerForm: false,
            superpowerFormClass: 'redForm'
         });
  }
}

checkDateofbirthInput = (value) => {
  if(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/.test(value)){
    this.setState({
            dateofbirthForm: true,
            dateofbirthFormClass: 'greenForm'
         });
  }else{
    this.setState({
            dateofbirthForm: false,
            dateofbirthFormClass: 'redForm'
         });
  }
}

  renderupdate(){
    return(
      <React.Fragment>
        <Card className='full padding_top'>
        <div style={{backgroundImage: "url(" + this.state.imgurl + ")"}} className={this.state.isOpacity ? 'card_image edit opacity_edit' : 'card_image edit opacity_edit_new_img'} ></div>
        <div className='icons-container'>
          <IoIosAddCircle className='float_left pointer' onClick={()=>{this.upload.click()}} />
          <IoIosCloseCircle className='float_right pointer' style={{display: this.state.image || this.state.imgName ? 'block' : 'none' }}  onClick={()=>{this.removeImage()}} />
        </div>
        <CardBody>
      <Form action="/cosmonauts/imgupload" method="post" encType="multipart/form-data" >
        <FormGroup>
          <Input type="text" name="firstname" id="firstname" placeholder={this.props.firstname}  className={this.state.firstnameFormClass} onChange={this.changeFirstnameInput.bind(this)} />
        </FormGroup>
        <FormGroup>
          <Input type="text" name="lastname" id="lastname" placeholder={this.props.lastname}  className={this.state.lastnameFormClass} onChange={this.changeLastnameInput.bind(this)} />
        </FormGroup>
        <FormGroup>
          <Input type="text" name="dateofbirth" id="dateofbirth" placeholder={this.props.dateofbirth}  className={this.state.dateofbirthFormClass} onChange={this.changeDateofbirthInput.bind(this)} />
        </FormGroup>
        
          <Input type="text" name="superpower" id="superpower" placeholder={this.props.superpower}  className={this.state.superpowerFormClass} onChange={this.changeSuperpowerInput.bind(this)} />
        
        <FormGroup>
        <input type="file" className="form-control" onChange={this.changeImgInput.bind(this)} ref={(ref) => this.upload = ref} style={{display: 'none'}} />
        </FormGroup>
        </Form>
        </CardBody>
        <div className='row padding_bottom' >
        <div className='col-2' ></div>
          <Button className="text-center col-4" onClick={this.saveUpdateButton}>Save</Button>
          <Button className="text-center col-4" onClick={this.dismissUpdateButton}>Cancel</Button>
        </div>
      </Card>
      </React.Fragment>
      )
  }

render() {
      return (
        this.renderupdate() 
      )
 }  
}
export default EditCosmonaut