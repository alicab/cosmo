import React from 'react';
import './App.css';
import { Card, CardBody, CardTitle, Button} from 'reactstrap';

class DeleteCosmonaut extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstname: this.props.firstname,
      lastname: this.props.lastname,
      dateofbirth: this.props.dateofbirth,
      superpower: this.props.superpower,
      showDelete: true
   };
  }

  deleteCosmonaut = () => {
    fetch('/cosmonauts/'+this.props.id, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
    }
    })
    .then(
      () => {
         this.props.getcosmonauts();
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  deleteButton = () => {
    this.deleteCosmonaut();
    this.setState({
            showDelete: false
         });
  }

  dismissButton = () => {
    this.props.changestatenotdelete();
  }

  renderdelete(){
    return(
      <React.Fragment>
        <Card style={{display: this.state.showDelete ? 'block' : 'none' }} className='full'>
        
        <CardBody className='full-row delete_middle'>
          <CardTitle>Are you sure you want to delete the cosmonaut? This operation is irreversible.</CardTitle>
        </CardBody>
        <div className='row padding_bottom delete_button' >
          <div className="col-2 col-sm-2 col-md-2 col-lg-2"></div>
          <Button color='danger' className="col-4 col-sm-4 col-md-4 col-lg-4" onClick={this.dismissButton}>Cancel</Button>
          <Button color='danger' className="col-4 col-sm-4 col-md-4 col-lg-4" onClick={this.deleteButton}>Delete</Button>
         <div className="col-2 col-sm-2 col-md-2 col-lg-2"></div>
        </div>  
         
      </Card>
      </React.Fragment>
      )
  }

render() {
      return (
        this.renderdelete() 
      )
 }  
}
export default DeleteCosmonaut