import React from 'react';
import './App.css';
import { Card, CardBody, Button, Form, FormGroup, Input } from 'reactstrap';
import { IoIosAddCircle, IoIosCloseCircle } from 'react-icons/io';

class NewCosmonaut extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      dateofbirth: '',
      superpower: '',
      firstnameForm: false,
      lastnameForm: false,
      dateofbirthForm: false,
      superpowerForm: false,
      firstnameFormClass: '',
      lastnameFormClass: '',
      dateofbirthFormClass: '',
      superpowerFormClass: '',
      image: null,
      imageurl: 'cosmonaut.jpeg',
      isOpacity: true
   };
  }

  postCosmonaut = () => { 
    if(this.state.firstnameFormClass===''){
      this.setState({
            firstnameFormClass: 'redForm'
         });
    }
    if(this.state.lastnameFormClass===''){
      this.setState({
            lastnameFormClass: 'redForm'
         });
    }
    if(this.state.dateofbirthFormClass===''){
      this.setState({
            dateofbirthFormClass: 'redForm'
         });
    }
    if(this.state.superpowerFormClass===''){
      this.setState({
            superpowerFormClass: 'redForm'
         });
    }
    if(this.state.firstnameForm && this.state.lastnameForm && this.state.dateofbirthForm && this.state.superpowerForm){ 
  if(this.state.image){
  const formData = new FormData();
  formData.append("upload_preset", "hqc427x0");
  formData.append("file", this.state.image);
  formData.append("tags", "cosmo");
  var imgName=Date.now()+Math.floor(Math.random() * 101).toString();
  formData.append("public_id", imgName); 

const options = {
  method: 'POST',
  body: formData
};

fetch('https://api.cloudinary.com/v1_1/dazg4q7xb/image/upload/', options)
  .then((res) => {
    return res.json(); 
  })
  .then((res) => {
    fetch('/cosmonauts/new', {
      method: 'POST',
      body: JSON.stringify({
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        dateofbirth: this.state.dateofbirth,
        superpower: this.state.superpower,
        imgName: imgName
      }),
      headers: {
        'Content-Type': 'application/json'
    }
    })
    .then(
      (res) => {
        this.props.getcosmonauts();
        this.props.removenew();
  })
  .catch(function (error) {
    console.log(error);
  });
})
}else{
  fetch('/cosmonauts/new', {
      method: 'POST',
      body: JSON.stringify({
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        dateofbirth: this.state.dateofbirth,
        superpower: this.state.superpower,
        imgName: ''
      }),
      headers: {
        'Content-Type': 'application/json'
    }
    })
    .then(
      (res) => {
        this.props.getcosmonauts();
        this.props.removenew();
  })
  .catch(function (error) {
    console.log(error);
  });
}
}
}
  removeImage = () => {
        this.setState({
            imgName: '',
            image: null,
            imageurl: 'cosmonaut.jpeg',
            isOpacity: true
         });
}

  saveNewButton = () => {
      this.postCosmonaut();
  }

  dismissNewButton = () => {
    this.props.removenew();
  }

  changeFirstnameInput = (event) => {
    this.setState({
            firstname: event.target.value
         }, ()=>{
          this.checkFirstnameInput(this.state.firstname);
         });
  }

  changeLastnameInput = (event) => {
    this.setState({
            lastname: event.target.value
         }, ()=>{
          this.checkLastnameInput(this.state.lastname);
         });
  }

  changeDateofbirthInput = (event) => {
    this.setState({
            dateofbirth: event.target.value
         }, ()=>{
          this.checkDateofbirthInput(this.state.dateofbirth);
         });
  }

  changeSuperpowerInput = (event) => {
    this.setState({
            superpower: event.target.value
         }, ()=>{
          this.checkSuperpowerInput(this.state.superpower);
         });
  }

 changeImgInput = (event) => {
    this.setState({
            image: event.target.files[0],
            imageurl: URL.createObjectURL(event.target.files[0]),
            isOpacity: false
          });
  }

checkFirstnameInput = (value) => {
  if(/^[a-zA-Z0-9 ]{1,20}$/.test(value)){
    this.setState({
            firstnameForm: true,
            firstnameFormClass: 'greenForm'
         });
  }else{
    this.setState({
            firstnameForm: false,
            firstnameFormClass: 'redForm'
         });
  }
}

checkLastnameInput = (value) => {
  if(/^[a-zA-Z0-9 ]{1,20}$/.test(value)){
    this.setState({
            lastnameForm: true,
            lastnameFormClass: 'greenForm'
         });
  }else{
    this.setState({
            lastnameForm: false,
            lastnameFormClass: 'redForm'
         });
  }
}

checkSuperpowerInput = (value) => {
  if(/^[a-zA-Z0-9 ]{1,20}$/.test(value)){
    this.setState({
            superpowerForm: true,
            superpowerFormClass: 'greenForm'
         });
  }else{
    this.setState({
            superpowerForm: false,
            superpowerFormClass: 'redForm'
         });
  }
}

checkDateofbirthInput = (value) => {
  if(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/.test(value)){
    this.setState({
            dateofbirthForm: true,
            dateofbirthFormClass: 'greenForm'
         });
  }else{
    this.setState({
            dateofbirthForm: false,
            dateofbirthFormClass: 'redForm'
         });
  }
}

 rendernew(){
    return(
      <React.Fragment>
        <Card className='full padding_top' >
        <div style={{backgroundImage: "url(" + this.state.imageurl + ")"}} className={this.state.isOpacity ? 'card_image edit opacity_edit' : 'card_image edit opacity_edit_new_img'} ></div>
        <div className='icons-container'>
          <IoIosAddCircle className='float_left pointer' onClick={()=>{this.upload.click()}} />
          <IoIosCloseCircle className='float_right pointer' style={{display: this.state.image ? 'block' : 'none' }}  onClick={()=>{this.removeImage()}} />
        </div>
        <CardBody>
      <Form action="/cosmonauts/imgupload" method="post" encType="multipart/form-data">
      <FormGroup>
          <Input type="text" name="firstname" id="firstname" placeholder='Eleonore' maxLength='20' className={this.state.firstnameFormClass} onChange={this.changeFirstnameInput.bind(this)}/>
      </FormGroup>
<FormGroup>
          <Input type="text" name="lastname" id="lastname" placeholder='Black' maxLength='20' className={this.state.lastnameFormClass} onChange={this.changeLastnameInput.bind(this)}/>
        </FormGroup>
        <FormGroup>
          <Input type="text" name="dateofbirth" id="dateofbirth" placeholder='1980-02-22' maxLength='10'  className={this.state.dateofbirthFormClass} onChange={this.changeDateofbirthInput.bind(this)}/>
        </FormGroup>
          <Input type="text" name="superpower" id="superpower" placeholder='stargazing' maxLength='20'  className={this.state.superpowerFormClass} onChange={this.changeSuperpowerInput.bind(this)}/>
        
        <FormGroup>
        <input type="file" className="form-control" name="avatar" accept="image/png, image/jpeg" onChange={this.changeImgInput.bind(this)} ref={(ref) => this.upload = ref} style={{display: 'none'}} />
        </FormGroup>
          </Form>
        </CardBody>
        <div className='row padding_bottom'>
        <div className='col-2' ></div>
        <Button className="text-center col-4" onClick={this.saveNewButton}>Save</Button>
          <Button className="text-center col-4" onClick={this.dismissNewButton}>Cancel</Button>
          <div className='col-2' ></div>
          </div>
      </Card>
      </React.Fragment>
      )
  }

render() {
      return (
        this.rendernew()
      )
 }  
}
export default NewCosmonaut