import React from 'react';
import './App.css';
import { Navbar, Nav, NavItem, NavbarBrand, Button } from 'reactstrap';

class MyNavbar extends React.Component {

render() {
      return (
      	<React.Fragment>
      		<Navbar color="dark" dark expand="md" className='white'>
            	<NavbarBrand href="/" className='header_font mr-auto d-none d-sm-block'>COSMOBASE</NavbarBrand>
      			<Nav className="ml-auto d-none d-sm-block" navbar>
              		<NavItem>
              			<Button color="danger" className='' onClick={this.props.addcosmo}>Add new cosmonaut</Button>
              		</NavItem>
              	</Nav>

              	<NavbarBrand href="/" className='header_font m-auto d-xs-block d-sm-none'>COSMOBASE</NavbarBrand>
      			<Nav className="m-auto d-xs-block d-sm-none" navbar>
              		<NavItem>
              			<Button color="danger" className='' onClick={this.props.addcosmo}>Add new cosmonaut</Button>
              		</NavItem>
              	</Nav>
       		</Navbar>
       </React.Fragment>
      )
 }  
}
export default MyNavbar