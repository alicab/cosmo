import React from 'react';
import './App.css';
import { Col } from 'reactstrap';
import Cosmonaut from './cosmonaut.js';
import NewCosmonaut from './newcosmonaut.js'

class Cosmolist extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      getLoading: true
   };
  }

  getCosmonauts = ()=>{
    fetch('/cosmonauts', {
         method: 'GET'
      })
      .then((response) => response.json())
      .then((responseJson) => { 
         this.setState({
            data: responseJson
         });
      })
      .then(
      () => {
        this.setState({ 
            getLoading: false
         });
        this.setState({ 
            getLoading: true
         });
  })
      .catch((error) => {
         console.error(error);
      });
  }

   componentDidMount(){
      this.getCosmonauts();
   }

  changeImageUploaded = () => {
    this.setState({ 
            imageUploaded: false
         })
  }

   renderCosmoList = () => {
    return (this.state.data.map((cosmonaut) => {return (
        <Col xs="12" sm="6" md="6" lg="3" xl="3" key={cosmonaut._id} className='padding_row' >
        <Cosmonaut  
              id={cosmonaut._id} 
              imgname={cosmonaut.imgName} 
              firstname={cosmonaut.firstname} 
              lastname={cosmonaut.lastname} 
              dateofbirth={cosmonaut.dateofbirth} 
              superpower={cosmonaut.superpower} 
              getcosmonauts={this.getCosmonauts} />
        </Col>
        )}));
  }

render() {
      return(
        <React.Fragment>
        {this.props.addnewcosmo ? <Col xs="12" sm="6" md="6" lg="3" xl="3" className='padding_row'><NewCosmonaut 
                                                    removenew={this.props.removecosmo}
                                                    getcosmonauts={this.getCosmonauts} /></Col> : null }
        {this.renderCosmoList()}
        </React.Fragment>
        );
   }

}
export default Cosmolist;