import React from 'react';
import './App.js';
import MyNavbar from './navbar.js'
import Cosmolist from './cosmolist.js'
import { Container, Row} from 'reactstrap';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      addnewcosmo: false
   };
  }

  addnew = () => {
    this.setState({
            addnewcosmo: true
         });
  }

  removenew = () => {
    this.setState({
            addnewcosmo: false
         });
  }

render() {
      return (
        <React.Fragment>
        <MyNavbar addcosmo={this.addnew} />
        <Container>
        <Row>
        <Cosmolist addnewcosmo={this.state.addnewcosmo} removecosmo={this.removenew} />
        </Row>
        </Container>
        </React.Fragment>
      )
 }  
}
export default App