let mongoose = require('mongoose')

let cosmonautSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  dateofbirth: String,
  superpower: String,
  imgName: String

})

module.exports = mongoose.model('Cosmonaut', cosmonautSchema)