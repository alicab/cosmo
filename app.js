var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors');

var cosmonauts = require('./routes/cosmonauts');

var port = process.env.PORT || 5000; 

var app = express();

app.use(cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST", "PUT", "DELETE"],
    allowedHeaders: ["Content-Type", "Authorization"]
}));

app.use(express.static(path.join(__dirname, 'client/build')))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/cosmonauts', cosmonauts);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/build/index.html'))
})

app.listen(port, function(){
    console.log('Server started on port '+port);
});